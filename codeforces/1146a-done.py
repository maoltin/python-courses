inputString = input()
# inputString = 'aabbbbbbbbbba'

numberOfA = inputString.count('a')
lengthOfString = len(inputString)
# print(numberOfA)
maxGoodStringIfLong = numberOfA+(numberOfA-1)
# print(maxGoodStringIfLong)

if lengthOfString>maxGoodStringIfLong:
  print(maxGoodStringIfLong)
else:
  print(lengthOfString)