letterGoodness = [0.0817, 0.0149, 0.0278, 0.0425, 0.127, 0.0223, 0.0202, 0.0609, 0.0697, 0.0015, 0.0077, 0.0402, 0.0241, 0.0675, 0.0751, 0.0193, 0.0009, 0.0599, 0.0633, 0.0906, 0.0276, 0.0098, 0.0236, 0.0015, 0.0197, 0.0007]
print(letterGoodness) #testing... it's predefined
input_text = 'LQKP OG CV GKIJV DA VJG BQQ'
best_shift = 0
all_texts = []
max_goodness = 0

def getNewLetter(letter, shift):
  index = (ord(letter) - 65)
  new_index = 0
  outLetter = {}
  if (ord(letter)==32):
    new_letter=letter
    thisLetterGoodness=0
  else:
    new_index = (index+shift)%26+65
    new_letter = chr(new_index)
    thisLetterGoodness = letterGoodness[new_index-65]
  outLetter['letter'] = new_letter
  outLetter['goodness'] = thisLetterGoodness
  return outLetter

def checkText(text, shift, max_goodness):
  goodness = 0
  new_text = ''
  outText = {}
  outText['row'] = ''
  outText['goodness'] = 0
  for letter in text:
    new_letter = getNewLetter(letter, shift) 
    outText['row'] += new_letter['letter']
    outText['goodness'] += new_letter['goodness']
  return outText

for shift in range(26):
  new_text = checkText(input_text, shift, max_goodness)
  all_texts.append(new_text['row'])
  if new_text['goodness'] > max_goodness:
    max_goodness = new_text['goodness']
    best_shift = shift
print(all_texts[best_shift])

# letterGoodness = [0.0817, 0.0149, 0.0278, 0.0425, 0.127, 0.0223, 0.0202, 0.0609, 0.0697, 0.0015, 0.0077, 0.0402, 0.0241, 0.0675, 0.0751, 0.0193, 0.0009, 0.0599, 0.0633, 0.0906, 0.0276, 0.0098, 0.0236, 0.0015, 0.0197, 0.0007]
# print(letterGoodness) #testing... it's predefined
# input_text = 'A HELLO MY FRIENDS'
# best_shift = 0
# all_texts = []
# max_goodness = 0

# def getNewLetter(letter, shift):
#   index = (ord(letter) - 65)
#   letterArr = []
#   new_index = 0
#   if (ord(letter)==32):
#     new_letter=letter
#     thisLetterGoodness=0
#   else:
#     new_index = (index+shift)%26+65
#     new_letter = chr(new_index)
#     thisLetterGoodness = letterGoodness[new_index-65]
#   letterArr.append(new_letter)
#   letterArr.append(thisLetterGoodness)
#   return letterArr

# for shift in range(26):
#   goodness = 0
#   new_text = ''
#   for letter in input_text:
#     new_letter = getNewLetter(letter, shift) 
#     new_text += new_letter[0]
#     goodness += new_letter[1]
#   if goodness > max_goodness:
#     max_goodness = goodness
#     best_shift = shift
#   all_texts.append(new_text)
#   # print(new_text, goodness)
# print(all_texts[best_shift])