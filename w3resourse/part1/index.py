# if a>0:
# a=1
#           print(a)
# Twinkle, twinkle, little star,
# 	How I wonder what you are!
# 		Up above the world so high,
# 		Like a diamond in the sky.
# Twinkle, twinkle, little star,
# 	How I wonder what you are


# 1
formatted = "Twinkle, twinkle, little star,\n\t " \
        "How I wonder what you are! \n\t" \
            "Up above the world so high,\n\t\t" \
            "Like a diamond in the sky.\n" \
        "Twinkle, twinkle, little star,\n\t" \
            " How I wonder what you are"
print(formatted)

# 2
import sys
print(sys.version)

# 3
import datetime
now = datetime.datetime.now()
print("1")
print("Current date and time :", now.strftime("%Y-%m-%d %H:%M:%S"))
# 2014-07-05 14:34:14

# 4
