# # 4.1
# guess_me = 7
# if guess_me < 7:
#   print('too low!')
# elif guess_me > 7:
#   print('too high!')
# else:
#   print('just right')

# # 4.2
# guess_me = 7
# start = 1
# while True:
#   if start > guess_me:
#     print('oops')
#     break
#   elif start == guess_me:
#     print('found it')
#     break
#   else:
#     start += 1
#     print('too low!')

# # 4.3
# list = [3, 2, 1, 0]
# for i in range (0, len(list)):
#   print(list[i])

# # 4.4
# even_numbers = [number for number in range(10) if (number%2) == 0]
# print('even_numbers:', even_numbers)

# # 4.5
# squares = {number:number**2 for number in range(10)}
# print('squares: ', squares)

# # 4.6
# odd_numbers = {number for number in range(10) if (number%2) != 0}
# print(odd_numbers)

# # 4.7
# generator = (('Got ', number) for number in range(10))
# for i in generator:
#   print(i)

# # 4.8
# def good():
#   return ['Harry', 'Ron', 'Hermione']
# print(good())

# # 4.9
# def get_odds():
#   return [number for number in range(10) if (number%2)==0]
# for i in get_odds():
#   if (i==2):
#     print(get_odds()[i])

# 4.10
def decorated_function():
  print('this is decorated fuction!')
def test(decorated_function):
  print('start')
  decorated_function()
  print('end')
test(decorated_function)
# how to call decorated_function from test with arguments?

# 4.11
# Define an exception called OopsException. Raise this exception to see what happens
# Then write the code to catch this exception and print 'Caught an oops'.

# class OopsException(Exception):
#   pass

# 4.12
titles = ['Creature of Habit', 'Crewel Fate']
plots = ['A nun turns into a monster', 'A haunted yarn shop']
movies = {}
for title, plot in zip(titles, plots):
  print('title:', title,'plot:',plot)
print(movies)