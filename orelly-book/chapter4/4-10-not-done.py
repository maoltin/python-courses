def test(func):
  def new_func(*args, **kwargs): 
    print('start')
    result = func(*args, **kwargs)
    print('end')
    return result
  return new_func

def greeting(name):
  print("Greetings," + name)

greeting('bob')
test(greeting)
greeting('bob')


# def myFunc(a, b):
#   print(a+b)

# myFunc(1,5)
# myFunc(1,5)

# test(myFunc)

# myFunc(1,5)
# myFunc(1,5)