# 4.11
# Define an exception called OopsException. Raise this exception to see what happens
# Then write the code to catch this exception and print 'Caught an oops'.

class OopsException(Exception):
  pass

raise OopsException()

try:
  raise OopsException
except OopsException:
  print('Caught an oops')