years_list = []
year = birth_year = 1900
i = 0
while i<=5:
  years_list.append(year)
  year += 1
  i += 1
print ('years_list: ',years_list)
print ('3rd year: ', years_list[3])
print ('Oldest: ', max(years_list))

things = ["mozzarella", "cinderella", "salmonella"]
print('things: ', things)

for i in range(0, len(things)):
  things[i] = things[i].upper()
print('things: ', things)

# 3.7
things.remove("SALMONELLA")
print('things: ', things)

# 3.8
surprise = ["Groucho", "Chico", "Harpo"]

# 3.9
for i in range(0, len(surprise)): 
  surprise[i] = surprise[i].lower()
print('surprise: ', surprise)

surprise = surprise[::-1]
print('surprise: ', surprise)

for i in range(0, len(surprise)):
  surprise[i] = surprise[i].upper()
print('surprise: ', surprise)

# 3.10
e2f = {
  "dog": "chien",
  "cat": "chat",
  "walrus": "morse"
}
print('e2f: ', e2f)

# 3.11
print('walrus: ', e2f['walrus'])

# 3.12
pairs = (e2f.items())
f2e = {}
print('pairs: ', pairs)

for i in range(0, len(pairs)):
  pairs[i] = pairs[i][::-1]
f2e = dict(pairs)
print('f2e: ', f2e)

# 3.13
print('chien:', f2e['chien'])

# 3.14
print(e2f.keys())

# 3.15
cats = ['Henri', 'Grumpy', 'Lucy']
animals = {'cats': cats, 'octopi': {}, 'emus': {}}
other = {}
life = {'animals': animals, 'plants': '', 'other': other}
print('life: ', life)

# 3.16
print('life top-level keys: ', life.keys())

# 3.17
print(life['animals'].keys())

# 3.18
print(life['animals']['cats'])