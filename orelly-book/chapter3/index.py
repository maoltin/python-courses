# # 3.10
# e2f = {
#   "dog": "chien",
#   "cat": "chat",
#   "walrus": "morse"
# }
# # print('e2f: ', e2f)

# # # 3.11
# # print('walrus: ', e2f['walrus'])

# # 3.12

# f2e = {}
# for c in e2f.items():
#   # print(english, french)
#   f2e[c[1]] = c[0]
# print(f2e)
#   # print(english, french)
# # print(f2e)

# print(f2e['chien'])

# print('KEYS: ', e2f.keys())

# # 3.15
# cats = ['Henri', 'Grumpy', 'Lucy']
# animals = {'cats': cats, 'octopi': {}, 'emus': {}}
# other = {}
# life = {'animals': animals, 'plants': {}, 'other': other}
# print('life: ', life)

# # 3.16
# print('life top-level keys: ', life.keys())

# # 3.17
# print(life['animals'].keys())

# # 3.18
# print(life['animals']['cats'])

life = {'animals': {'cats': ['Henri', 'Grumpy', 'Lucy'], 'octopi': '', 'emus': {}}, 'plants': '', 'other': {}}
# print(typeof(life['octopi']))
# 3.16
print('life top-level keys: ', life.keys())

# 3.17
print(life['animals'].keys())
print(list(life['animals']))

# 3.18
print(life['animals']['cats'])