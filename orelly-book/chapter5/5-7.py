# 5.7. Make a defaultdict called dict_of_lists and pass it the argument list.
# Make the list dict_of_lists['a'] and append the value 
# 'something for a' to it in one assignment. Print dict_of_lists['a'].

import collections

dict_of_lists = collections.defaultdict(list)
dict_of_lists['a'].append('something for a')
# dict_of_lists['a'] = 'something for a'
print(dict_of_lists['a'])