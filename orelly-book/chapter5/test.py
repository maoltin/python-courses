periodic_table = {'Hydrogen': 1, 'Helium': 2}
print(periodic_table)

carbon = periodic_table.setdefault('Carbon', 12)
print(periodic_table)

helium = periodic_table.setdefault('Helium', 947)
print(periodic_table)

from collections import defaultdict
periodic_table = defaultdict(int)
print(periodic_table)

periodic_table['Hydrogen'] = 1
periodic_table['Lead']
periodic_table = defaultdict(int)
print(periodic_table)

from collections import defaultdict
def no_idea():
  return 'Huh?'
# no_idea = 'Huh!'
bestiary = defaultdict(no_idea)
bestiary['A'] = 'Abominable Snowman'
bestiary['B'] = 'Basilisk'
print(bestiary['A'])
print(bestiary['B'])
print(bestiary['C'])