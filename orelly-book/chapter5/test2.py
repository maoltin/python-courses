def palindrome(word):
  from collections import deque
  dq = deque(word)
  while len(dq) > 1:
    if dq.popleft() != dq.pop():
      return False
  return True
print(palindrome('a'))
print(palindrome('racecar'))
print(palindrome(''))
print(palindrome('radar'))
print(palindrome('halibut'))

def another_palindrome(word):
  return word == word[::-1]
print(another_palindrome('radar'))
print(another_palindrome('halibut'))

import itertools
for item in itertools.chain([1, 2], ['a', [1,2,[1,2,{'s':'s',2:2}]]]):
  print(item)

# import itertools
# for item in itertools.cycle([1, 2]):
#   print(item)

from pprint import pprint
quotes = dict([
('Moe', 'A wise guy, huh?'),
('Larry', 'Ow!'),
('Curly', 'Nyuk nyuk!'),
])
print(quotes)
pprint(quotes)