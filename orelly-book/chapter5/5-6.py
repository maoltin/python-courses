# 5.6. Make an OrderedDict called fancy from the same pairs listed in 5.5 
# and print it.
# Did it print in the same order as plain?

from pprint import pprint as myPrint
fancy = dict({'a': 1, 'b': 2, 'c':3})
print(fancy)
myPrint(fancy)